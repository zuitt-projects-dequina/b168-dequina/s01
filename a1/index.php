<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <style>
        body {
            border: 1px solid black;
            margin-left: 10%;
            margin-right: 10%;
            padding-left: 1rem;
            padding-right: 1rem;
            text-align: center;
        }
        
    </style>
    <title>S01 Activity</title>
</head>
<body>
    <h1>Full Address</h1>

    <p><?php echo getFullAddress('Philippines', 'Metro Manila', 'Quezon City', '3F Caswynn Bldg., Timog Avenue') ?></p>
    <p><?php echo getFullAddress('Philippines', 'Metro Manila', 'Makati City', '3F Enzo Bldg., Buendia Avenue') ?></p>

    <h1>Letter-Based Grading</h1>

    <p><?php echo getLetterGrade(87) ?></p>
    <p><?php echo getLetterGrade(94) ?></p>
    <p><?php echo getLetterGrade(74) ?></p>
</body>
</html>