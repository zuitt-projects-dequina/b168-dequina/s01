<!-- to link the external php code -->
<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>S01: PHP Basics and Selection Control Structures</title>
</head>
<body>
    <h1>Echoing Values</h1>

    <!-- between the <///> we can put all PHP codes -->
    <p><?php echo 'Hello Batch 168!'; ?></p>

    <!-- to see the results, go to Browser, then type the directory of the file starting with htdocs: http://localhost/b168/s01/d1/index.php -->

    <p><?php echo 'Good day $name! Your given email is $email.'; ?></p>

    <!-- to use the variables in the code.php, double quotes must be used-->
    <p><?php echo "Good day $name! Your given email is $email."; ?></p>

    <p><?php echo PI; ?></p>



    <!-- DATA TYPES -->
    
    <p><?php echo "State is $state in $country."; ?></p>
    <p><?php echo "Dot Notation: Your address is $address."; ?></p>
    <p><?php echo "Double Quotation: Your address is $addressTwo."; ?></p>

    <!-- Echoing Boolean and Null Variables -->
    <!-- normal echoing of boolean and null variables will not appear in the web output -->
    <p><?php echo "Boolean: $hasTravelledAbroad"; ?></p>
    <p><?php echo "Null: $spouse"; ?></p>
    
    <!-- to get the type of variables -->
    <!-- gettype() function returns the type of a variable -->
    <p><?php echo gettype($hasTravelledAbroad); ?></p>
    <p><?php echo gettype($spouse); ?></p>

    <!-- var_dump() function returns the value of the variable -->
    <p><?php echo var_dump($hasTravelledAbroad); ?></p>
    <p><?php echo var_dump($spouse); ?></p>

    <!-- Echoing an Array element -->
    <p><?php echo "Array Element: index[0] = $grades[0]"; ?></p>
    <p><?php echo "Array Element: index[3] = $grades[3]"; ?></p>

    <!-- Echoing an Object Properties -->
    <p><?php echo ">>> Echoing Object Properties <<<" ?></p>
    <p><?php echo $gradesObj->firstGrading ?></p>
    <p><?php echo $personObj->address->state ?></p>

    <h1>Operators</h1>

    <p>X: <?php echo $x; ?></p>
    <p>y: <?php echo $y; ?></p>

    <p>Is Legal Age: <?php echo var_dump($isLegalAge) ?></p>
    <p>Is Registered: <?php echo var_dump($isRegistered) ?></p>

    <p>Sum: <?php echo $x + $y ?></p>
    <p>Difference: <?php echo $x - $y ?></p>
    <p>Product: <?php echo $x * $y ?></p>
    <p>Quotient: <?php echo $x / $y ?></p>

    <h2>Equality Operators</h2>

    <p>Loose Equality: <?php echo var_dump($x == '56.2') ?></p>
    <p>Strict Equality: <?php echo var_dump($x === '56.2') ?></p>
    <p>Loose Equality: <?php echo var_dump($x != '56.2') ?></p>
    <p>Strict Equality: <?php echo var_dump($x !== '56.2') ?></p>

    <h2>Greater/Lesser Operators</h2>

    <p>Is Lesser: <?php echo var_dump($x < $y); ?></p>
    <p>Is Greater: <?php echo var_dump($x > $y); ?></p>
    
    <h2>Logical Operators</h2>

    <!-- logical operator: && || or by text 'and' 'or' -->
    <p>Are all Requirements Met: <?php echo var_dump($isLegalAge && $isRegistered) ?></p>
    <p>Are some Requirements Met: <?php echo var_dump($isLegalAge || $isRegistered) ?></p>
    <p>Are some Requirements not Met: <?php echo var_dump(!$isLegalAge and !$isRegistered) ?></p>

    <h1>Functions</h1>

    <p>Full Name: <?php echo getFullName('John', 'D', 'Smith') ?></p>

    <h1>If-ElseIf-Else Statement</h1>

    <p><?php echo determineTyphoonIntensity(12) ?></p>

    <h1>Ternary Operator</h1>

    <p>isUnderAge?</p>
    <p>78: <?php echo var_dump(isUnderAge(78)); ?></p>
    <p>17: <?php echo var_dump(isUnderAge(17)); ?></p>

    <h1>Switch</h1>
    <p><?php echo determineComputerUser(4); ?></p>
    <p><?php echo determineComputerUser(8); ?></p>

    <h1>Try-Catch-Finally</h1>

    <p><?php echo greeting('Hello'); ?></p>

</body>
</html>